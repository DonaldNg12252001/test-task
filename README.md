Instructions for Code - Ka Ho(Donald) Ng

Must use two node.js command prompt and have them in the proper directory (`.\Test Task - Ka Ho (Donald) Ng\emailtask` and `.\Test Task - Ka Ho (Donald) Ng`). One command prompt will run react.js and the other 
will be running the node backend.

To use react.js you must input npm start with the directory changed to `.\Test Task - Ka Ho (Donald) Ng\emailtask`

To run the node backend, server.js you must input node server.js or nodemon server.js with the directory changed to `.\Test Task - Ka Ho (Donald) Ng`

Frontend is held in App.js in email task folder.
Backend is held in server.js in root directory of Test Task folder.

Direct send will send the email and review button will show user what the email would look like when sent.