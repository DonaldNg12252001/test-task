import React, { Component } from 'react';
import logo from './Logo.PNG';
import './App.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';


class Email extends Component {
  constructor(props) {
    super(props);
    this.resetstate={invoice:"",
    recipient:"",
    Address:"",
    Date:"",
    Reason:"",
    invoicetotal:"",
    Description:"",
    cad:""};
    this.reviewstate={invoice1:"",
    recipient1:"",
    Address1:"",
    Date1:"",
    Reason1:"",
    invoicetotal1:"",
    Description1:"",
    cad1:""};
//Allows user to reset form
    this.state = this.resetstate;
  };
//Each Handle changes makes each variable equal to what the user has put in.
  handleinvoiceChange = (event) => {
     this.setState({invoice: event.target.value});
  }
  handlerecipientChange = (event) => {
     this.setState({recipient: event.target.value});
  }
  handleAddressChange = (event) => {
     this.setState({Address: event.target.value});
  }
  handleDateChange = (event) => {
     this.setState({Date: event.target.value});
  }
  handleReasonChange = (event) => {
     this.setState({Reason: event.target.value});
  }
  handleinvoicetotalChange = (event) => {
     this.setState({invoicetotal: event.target.value});
  }
  handleDescriptionChange = (event) => {
     this.setState({Description: event.target.value});
  }
  handlecadChange = (event) => {
     this.setState({cad: event.target.value});
  }

//when the Direct Send button is pressed, this function will post the information to the node server.
//Axios was used because it is a more simple and reliable javascript livrary that performs http request in a simple way compared to other http request method
//such as react's fetch. Axios works a lot better with node as it only requires the method (post and get) and the node url to be able to send data to backend
  handleSubmit = (event) => {
    event.preventDefault();
        axios({
            method: "POST",
            url:"http://localhost:8080/",
            data: {
              invoice:this.state.invoice,
              recipient:this.state.recipient,
              Address:this.state.Address,
              Date:this.state.Date,
              reason:this.state.Reason,
              total:this.state.invoicetotal,
              description:this.state.Description,
              price:this.state.cad
            }
//If its successful then use react toastify to notifiy/send a popup saying successfully sent and resets forms
        }).then(() => {
          //handle success
          toast.success("Successfully Sent!")
          this.setState (this.resetstate);
          this.setState (this.reviewstate);
        })
//If an error was found and the email could not be sent then use react toastify to notifiy/send a popup saying Failed to send.
        .catch(function (response) {
          //handle error
          console.log(response);
          toast.error("Failed to Send!")
        });
    };

//When User wants to review the email, this function will display all the information that has been given in to email format
    handleReview = (event) => {
      this.setState({invoice1:this.state.invoice,
      recipient1:this.state.recipient,
      Address1:this.state.Address,
      Date1:this.state.Date,
      Reason1:this.state.Reason,
      invoicetotal1:this.state.invoicetotal,
      Description1:this.state.Description,
      cad1:this.state.cad});
    };
//Contains all user input for email
//Contains 2 buttons, Direct send and review. Direct send will send the information to backend server then it will be sent to email destination. Review will fill in template of the email at the bottom
//to allow user to review and edit their information.
  render(){
    return (
      <div className="formatpage">
      <ToastContainer />
        <div className="rowtitle">
        <h1>Email Review/Edit and Send Service</h1>
        </div>
        <div className="format">
        <form onSubmit={this.handleSubmit}>
        <div className="sameline">
          <label for="invoice">INVOICE: #</label>
          <br />
          <input type='text' id="invoice" name="invoice" value={this.state.invoice} onChange={this.handleinvoiceChange}/>
        </div>
        <div className="sameline">
          <label for="recipient">TO: </label>
          <br />
          <input type='text' id="recipient" name="recipient" value={this.state.recipient} onChange={this.handlerecipientChange}/>
        </div>
        <div className="sameline">
          <label for="Address">Address: </label>
          <br />
          <input type='text' id="Address" name="Address" value={this.state.Address} onChange={this.handleAddressChange}/>
        </div>
        <div className="sameline">
          <label for="Date">Date: </label>
          <br />
          <input type='Date' id="Date" name="Date" value={this.state.Date} onChange={this.handleDateChange}/>
        </div>
        <div className="sameline">
          <label for="reason">Reason for Payment: </label>
          <br />
          <input type='text' id="reason" name="reason" value={this.state.Reason} onChange={this.handleReasonChange}/>
        </div>
        <div className="sameline">
          <label for="total"><b>Invoice Total: </b></label>
          <br />
          <input type='text' id="total" name="total" value={this.state.invoicetotal} onChange={this.handleinvoicetotalChange}/>
        </div>
        <div>
        <label className="sameline" for="description"><b>Description</b></label><br />
        <textarea className="box" id="description" name="description" rows="8" cols="60" value={this.state.Description} onChange={this.handleDescriptionChange}></textarea>
        </div>
        <div className="sameline">
          <label for="price"><b>CAD$ </b></label>
          <br />
          <input type='text' id="price" name="price" value={this.state.cad} onChange={this.handlecadChange}/>
        </div>
        <div>
        <button className="button" type="submit">Direct Send </button>
        </div>
        </form>
        <button className="button" onClick={this.handleReview}>Review </button>
        </div>
        <div>
        <h1>Review</h1>
        <div className="column">
          <img src ={logo} alt="logo" height="225" width="225" />
        </div>
        <div className="column1">
          <p> RAPID DEVELOPERS INC.</p>
          <p> 1904-777 BAY ST,</p>
          <p> TORONTO ON M5B 2H7</p>
        </div>
        <h3 className="invoiceformat">INVOICE: #{this.state.invoice1}</h3>
        <p>TO: {this.state.recipient1}</p>
        <p>{this.state.Address1}</p>
        <p>Date: {this.state.Date1}</p>
        <p>{this.state.Reason1}</p>
        <p><b>Invoice Total: ${this.state.invoicetotal1}</b> (see details below)</p>
        <table>
          <tr>
            <th>Description</th>
            <th>CAD$</th>
          </tr>
          <tr>
            <td className="textformat">{this.state.Description1}</td>
            <td>{this.state.cad1}</td>
          </tr>
        </table>
        </div>
      </div>
    );
  }
}


export default Email;
