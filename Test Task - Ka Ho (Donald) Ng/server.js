require('dotenv').config({path: __dirname + '/.env'})
const express = require('express');
const nodemailer = require("nodemailer");
var app = express();
const port = process.env.PORT;

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin,X-Requested-With, Content-Type,Accept,content-type,application/json");
  next();
});

app.use(express.json())

app.use('/', function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader("Access-Control-Allow-Headers", "*");
    next();
});

//Where the email will be sent
const transport = {
  host: process.env.host,
  port: process.env.smtp,
  auth: {
    user: process.env.user,
    pass: process.env.pass
  }
};

//Error Handling. If an error has appears, send error to console. If no errors has occured, tell user the server is ready.
const transporter = nodemailer.createTransport(transport);
  transporter.verify((error, success) => {
    if(error) {
      console.error(error)
    } else {
      console.log('users ready to mail')
    }
  });

//Post/receive the information and puts all the required information into a proper format and send.
  app.post('/', (req,res, next) => {
      const mail = {
        from: "donald.kh.ng@gmail.com",
        to: "test-developers@tbecanada.com",
        subject: "EMAIL TEST TASK",
        text: `
        INVOICE:#${req.body.invoice}
        TO: ${req.body.recipient}
        ${req.body.Address}
        DATE: ${req.body.Date}
        ${req.body.reason}
        Invoice total:$${req.body.total}
        Description:${req.body.description}
        CAD$: ${req.body.price}
        `,
        attachments: [{
        filename: 'Logo.PNG',
        path: __dirname +'/emailtask/src/Logo.png',
        cid: 'logo'
        }],
        html:
        `<html>
        <style>
        table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        }
        </style>
        <div style="width:30%; float:left; margin-top:10px;display:inline-block;">
          <img height="225" width="225" src="cid:logo">
        </div>
        <div style="width:70%; float:right;font-size:30px;margin-top:10px;display:inline-block;">
          <p> RAPID DEVELOPERS INC.</p>
          <p> 1904-777 BAY ST,</p>
          <p> TORONTO ON M5B 2H7</p>
        </div>
        <br></br>
        <br></br>
        <div style="display:inline-block; width:50%;">
          <h3 style="color: #00ace6;">INVOICE: #${req.body.invoice}</h3>
          <p>TO: ${req.body.recipient}</p>
          <p>${req.body.Address}</p>
          <p>Date: ${req.body.Date}</p>
          <p>${req.body.reason}</p>
          <p><b>Invoice Total: $${req.body.total}</b> (see details below)</p>
          <table style="width:100%">
            <tr>
              <th>Description</th>
              <th>CAD$</th>
            </tr>
            <tr>
              <td style="white-space:pre-wrap;">${req.body.description}</td>
              <td>${req.body.price}</td>
            </tr>
          </table>
          </div>
        </html>`
      }
      transporter.sendMail(mail, (err,data) => {
            if(err) {
              throw err
              res.json({
                status: 'fail'
              })
            } else {
              res.json({
                status: 'success'
              })
            }
          })
    });

app.listen(port, () => console.log(`Listening on port ${port}`));
